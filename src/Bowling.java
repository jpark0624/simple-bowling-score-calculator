import java.util.*;

public class Bowling {

    public static void main (String[] args) {
        Bowling bowling = new Bowling();
        String input = "1 -/ X 5- 8/ 9- X 81 1- 4/X";
        System.out.println("Total score: " + bowling.settleTheScore(input));
    }

    private int score(int pos, String input) {
        try {
            String actual = input.substring(pos, pos + 1);
            if (actual.equals("X")) {
                return 10;
            } else if (actual.equals("/")) {
                return (10 - Integer.parseInt(input.substring(pos - 1, pos)));
            } else {
                return Integer.parseInt(input.substring(pos, pos + 1));
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    private int settleTheScore(String inputRaw) {
        String[] inputArray = inputRaw.split("\\s");
        String input = inputRaw.replace(" ", "").replace("-", "0");
        int total = 0;
        for (int i = 0; i < input.length(); i++) {
            total += this.score(i, input);
            if (i < input.length() - inputArray[9].length()) {
                if (input.substring(i, i + 1).equals("X")) {
                    total += (this.score(i + 1, input) + this.score(i + 2, input));
                } else if (input.substring(i, i + 1).equals("/")) {
                    total += this.score(i + 1, input);
                }
            }
        }
        return total;
    }
}